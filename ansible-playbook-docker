- name: Install Docker
  hosts: all
  become: yes
  tasks:
    - name: Update the apt package repo
      ansible.builtin.apt:
        update_cache: yes

    - name: Install required packages
      ansible.builtin.apt:
        name:
          - apt-transport-https
          - ca-certificates
          - curl
          - gnupg-agent
          - software-properties-common
        state: present

    - name: Create keyring directory
      ansible.builtin.file:
        path: /etc/apt/keyrings
        state: directory
        mode: '0755'

    - name: Add Docker official GPG key
      ansible.builtin.apt_key:
        url: https://download.docker.com/linux/ubuntu/gpg
        keyring: /etc/apt/keyrings/docker.gpg
        state: present

    - name: Set proper permissions on Docker GPG file
      ansible.builtin.file:
        path: /etc/apt/keyrings/docker.gpg
        mode: a+r

    - name: Set up the repository
      ansible.builtin.shell: echo "deb [arch=\"$(dpkg --print-architecture)\" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \"$(. /etc/os-release && echo \"$VERSION_CODENAME\")\" stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null

    - name: Update the apt package repo
      ansible.builtin.apt:
        update_cache: yes

    - name: Install Docker Engine
      ansible.builtin.apt:
        name: docker-ce
        state: present

    - name: Install Docker Compose
      ansible.builtin.get_url:
        url: https://github.com/docker/compose/releases/download/1.29.2/docker-compose-{{ ansible_system }}-{{ ansible_userspace_architecture }}
        dest: /usr/local/bin/docker-compose
        mode: 'u+x,g+x'
        validate_certs: yes

    - name: Ensure Docker service is running
      ansible.builtin.service:
        name: docker
        state: started
        enabled: yes

    - name: Add user to Docker group
      ansible.builtin.user:
        name: "{{ ansible_user_id }}"
        groups: docker
        append: yes
